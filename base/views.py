from django.shortcuts import render
from django.views.generic import TemplateView
# Create your views here.

class MainView(TemplateView):
    template_name = 'main.html'
    def get_context_data(self, **kwargs):
        return super().get_context_data(**kwargs)
